## About This Project

Hello! this my project course udemy about laravel vue. lets get started!

### Installation
1. git clone
2. <code>composer install</code>
3. setup your .env
5. <code>php artisan key:generate</code>
6. <code>php artisan config:cache</code>
7. <code>php artisan config:clear</code>
8. <code>php artisan migrate</code>
